import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_application/Screens/dashboard.dart';
import 'package:flutter_application/Screens/mysignup.dart';
import 'package:flutter_application/theme.dart';
import 'package:flutter_application/widgets/logIn.dart';
import 'package:flutter_application/widgets/primary_button.dart';

class LogInScreen extends StatelessWidget {
  const LogInScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(138, 110, 100, 80),
      body: Padding(
        
        padding: kDefaultPadding,
        child: Column(
          children: [
            const SizedBox(
              height: 120,
            ),
            Text(
              'KAKAPA',
              style: titleText,
            ),
            const SizedBox(
              height: 5,
            ),
            
            const SizedBox(
              height: 10,
            ),
            logIn(),
            const SizedBox(
              height: 20,
            ),
            const Text(
              'Forgot password?',
              style: TextStyle(
                color: kZambeziColor,
                fontSize: 14,
                decoration: TextDecoration.underline,
                decorationThickness: 1,
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const Dashboard(),
                  ),
                );
              },
              child: const PrimaryButton(
                buttonText: 'Log In',
              ),
            ),
            Row(
              children: [
                const Text(
                  'New to app?  ',
                  style: TextStyle(
                    color: Color.fromARGB(242, 174, 177, 182),
                  ),
                ),
                const SizedBox(
                  width: 5,
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const SignUpScreen(),
                      ),
                    );
                  },
                  child: Text(
                    'Create Account',
                    style: textButton.copyWith(
                      decoration: TextDecoration.underline,
                      decorationThickness: 1,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
